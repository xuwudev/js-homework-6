// 1. Екранування це метод за допомогою якого ми можемо уникнути синтаксичних помилок у коді.
// 2. function(), arrowfunction
// 3. hoisting це винесення у скоуп коду, який був продекларований після того, як йому надали значення. Наче так:)

function createNewUser(firstName, lastName) {
  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`
    },
    getAge: function () {
      const today = new Date()
      const birthDate = new Date(this.birthday)
      let age = today.getFullYear() - birthDate.getFullYear()
      const month = today.getMonth() - birthDate.getMonth()
      if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
        age--
      }
      return age
    },
    getPassword: function () {
      const birthYear = this.birthday.getFullYear()
      return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${birthYear}`
    },
  }

  const inputDate = prompt("Введіть дату народження у форматі dd.mm.yyyy")
  const [day, month, year] = inputDate.split(".")
  newUser.birthday = new Date(`${year}-${month}-${day}`)
  console.log(newUser)
  console.log("Вік користувача: ", newUser.getAge())
  console.log("Пароль користувача: ", newUser.getPassword())
}

createNewUser("maksym", "maslo")
